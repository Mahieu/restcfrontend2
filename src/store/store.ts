import { configureStore } from '@reduxjs/toolkit'
import countriesReducer from '../slicers/countries.slicer';
import authReducer from '../slicers/auth.slicer';
export default configureStore({
  reducer: {
    countries: countriesReducer,
    auth : authReducer
  }
})