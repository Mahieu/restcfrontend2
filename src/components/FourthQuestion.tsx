import React, { useState, useEffect } from 'react';
import { Sprite, Text } from '@inlet/react-pixi';
import { useTick } from '@inlet/react-pixi';
import { TextStyle } from 'pixi.js';
import { Game } from '../models/game.model';
import GameService from '../helpers/game.service';
import { useSelector } from 'react-redux';
import { selectIsLoggedIn } from '../slicers/auth.slicer';
const FourthQuestion: React.FC = () => {
  const isLoggedIn : boolean = useSelector(selectIsLoggedIn);
  const [game, setGame] = useState<Game>(new Game());
  const [sprite1Texture, setSprite1Texture] = useState<string>("../images/cherry.png");
  const [sprite2Texture, setSprite2Texture] = useState<string>("../images/lemon.png");
  const [sprite3Texture, setSprite3Texture] = useState<string>("../images/lemon.png");
  const [isSpinningSprite1, setIsSpinningSprite1] = useState<boolean>(false);
  const [isSpinningSprite2, setIsSpinningSprite2] = useState<boolean>(false);
  const [isSpinningSprite3, setIsSpinningSprite3] = useState<boolean>(false);
  const [isCurrentlyPlaying, setIsCurrentlyPlaying] = useState<boolean>(false);
  const [coins, setCoins] = useState<number>(30);
  const images : string[] = ["../images/apple.png", "../images/bananas.png", "../images/cherry.png", "../images/lemon.png"];
  const [counter, setCounter] = useState<number>(0);
  const [index, setIndex] = useState<number>(0);
  useEffect(() => {
    let tmpCoins = Number.parseInt(localStorage.getItem('coins') + "");
    setCoins(tmpCoins);
  }, []);
  function getSprite(fruit: number): string {
    let image: string = images[0];
    switch (fruit) {
      case 1:
        image = images[2];
        break;
      case 2:
        image = images[3];
        break;
      case 3:
        image = images[1];
        break;
      case 4:
        image = images[0];
        break;
      default:
        image = images[0];
        break;
    }
    return image;
  }
  useTick(delta => {
    if (delta === undefined) {
      delta = 1;
    }
    if (isCurrentlyPlaying) {
      if (counter % 5 === 0) {
        setIndex(index + 1);
        if (isSpinningSprite1) {
          setSprite1Texture(images[index % 3]);
        }
        if (isSpinningSprite2) {
          setSprite2Texture(images[(index + 1) % 3]);
        }
        if (isSpinningSprite3) {
          setSprite3Texture(images[(index + 2) % 3]);
        }
      }
      if (counter === 250) {

        setIsSpinningSprite1(false);
        setSprite1Texture(getSprite(game.result[0]));
      }
      if (counter === 375) {
        setIsSpinningSprite2(false);
        setSprite2Texture(getSprite(game.result[1]));
      }
      if (counter >= 500) {
        setCounter(0);
        setIndex(0);
        setIsSpinningSprite3(false);
        setIsCurrentlyPlaying(false);
        setSprite3Texture(getSprite(game.result[2]));
        setCoins(game.coins);
        localStorage.setItem('coins', game.coins.toString());
      }
      else {
        setCounter(counter + 1);
      }
    }
  }
  );
  function launchGame() {
    GameService.getResult().then((res: Game) => {
      setGame(res);
    });
    setIsCurrentlyPlaying(true);
    setIsSpinningSprite1(true);
    setIsSpinningSprite2(true);
    setIsSpinningSprite3(true);
    setCoins(coins - 1);
  }
  return (
    <>
      <Sprite image={sprite1Texture} x={25} y={95} visible={isLoggedIn} />
      <Sprite image={sprite2Texture} x={175} y={95} visible={isLoggedIn} />
      <Sprite image={sprite3Texture} x={325} y={95} visible={isLoggedIn} />
      <Sprite image="../images/machine.png" visible={isLoggedIn} />
      <Sprite image="../images/button.png" x={315} y={275} interactive={true} pointerdown={() => launchGame()} visible={isLoggedIn && coins > 0} />
      <Text text="Please login to play this game." visible={!isLoggedIn} anchor={0.5}
        x={250}
        y={250}
      />
      <Text
        text={coins.toString()}
        visible={isLoggedIn}
        anchor={0.5}
        x={100}
        y={315}
        style={
          new TextStyle({
            align: 'left',
            fontFamily: '"Harlow Solid Italic","Source Sans Pro", Helvetica, sans-serif',
            fontSize: 25,
            fontWeight: "400",
            fill: ['#ffffff', '#00ff99'], // gradient
            stroke: '#01d27e',
            strokeThickness: 5
          })
        } />
    </>
  );
}
export default FourthQuestion;