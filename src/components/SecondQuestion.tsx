import React, { useState } from "react";
import Badge from "react-bootstrap/Badge";
import "../styles/SecondQuestion.css";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import { Country } from "../models/country.model";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import CountryCard from "../components/CountryCard";
import CardColumns from "react-bootstrap/CardColumns";
import { ReactComponent as SearchSVG } from '../images/search.svg';
import { useDispatch, useSelector } from "react-redux";
import { fetchCountries, selectCountries } from '../slicers/countries.slicer';
const SecondQuestion: React.FC = () => {
  const dispatch = useDispatch();
  const countries : Country[] = useSelector(selectCountries);
  let index: number = 1;
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [searchTerms, setSearchTerms] = useState<string[]>([]);
  function loadData() : void {
    dispatch(fetchCountries(searchTerms));
  }
  function addSearch() : void {
    setSearchTerms(searchTerms.concat(searchTerm));
  }
  function removeSearch(selectedSt: string) : void {
    setSearchTerms(searchTerms.filter((st) => st !== selectedSt));
  }

  return (
    <div className="SecondQuestionContainer">
      <Container className="mt-3 w-75 mb-3">
        <Row>
          <Col>
            <InputGroup className="">
              <FormControl
                aria-describedby="basic-addon1"
                placeholder="Type a search term"
                onChange={(event) => setSearchTerm(event.target.value)}
              />
              <InputGroup.Append>
                <Button onClick={addSearch} variant="outline-success">
                  +
                </Button>
                <Button onClick={loadData} variant="outline-secondary">
                  <SearchSVG />
                </Button>
              </InputGroup.Append>
            </InputGroup>
          </Col>
        </Row>
        <Row>
          <Col>
            {searchTerms.map((st) => (
              <Badge
                pill
                variant="primary"
                className="searchTerm"
                onClick={() => removeSearch(st)}
                key={st + index++}
              >
                {st}
              </Badge>
            ))}
          </Col>
        </Row>
      </Container>
      <CardColumns>
        {countries.map((c: Country) => (
          <CountryCard key={c.name + index++} country={c} />
        ))}
      </CardColumns>
    </div>
  );
};

export default SecondQuestion;
