import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import FirstQuestion from "./FirstQuestion";
import SecondQuestion from "./SecondQuestion";
import ThirdQuestion from "./ThirdQuestion";
import FourthQuestion from "./FourthQuestion";
import '../styles/Menu.css';
import { Stage, Container } from "@inlet/react-pixi";
import Login from "./Login";
import { Button } from "react-bootstrap";
import { useSelector, useDispatch, Provider } from 'react-redux';
import { setIsLoggedIn, selectIsLoggedIn } from '../slicers/auth.slicer';
import store from '../store/store';
const Menu: React.FC = () => {
  const isLoggedIn : boolean = useSelector(selectIsLoggedIn);
  const dispatch = useDispatch();
  function setLoginState(val: boolean) : void {
    dispatch(setIsLoggedIn(false));
    if (!val) {
      localStorage.clear();
    }
  }
  return (
    <Router>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand>RestCountries</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Link to="/q1" className="nav-link">Question 1</Link>
            <Link to="/q2" className="nav-link">Question 2</Link>
            <Link to="/q3" className="nav-link">Question 3</Link>
            <Link to="/q4" className="nav-link">Question 4</Link>
          </Nav>
          <Nav>
            <Button variant="outline-danger" hidden={!isLoggedIn} onClick={() => setLoginState(false)}>Disconnect</Button>
            <Link to="/login" className="nav-link" hidden={isLoggedIn}>Login</Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Switch>
        <Route path="/q1">
          <FirstQuestion />
        </Route>
        <Route path="/q2">
          <SecondQuestion />
        </Route>
        <Route path="/q3">
          <ThirdQuestion />
        </Route>
        <Route path="/q4">
          <div className="FourthQuestionContainer">
            <Stage width={500} height={500} options={{ backgroundColor: 0xeef1f5 }}>
              <Container>
                <Provider store={store}>
                  <FourthQuestion />
                </Provider>
              </Container>
            </Stage>
          </div>
        </Route>
        <Route path="/login">
          <Login />
        </Route>
      </Switch>
    </Router>
  );
};
export default Menu;
