import React, { useState } from "react";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import CountryService from "../helpers/country.service";
import { Country } from "../models/country.model";
import "../styles/FirstQuestion.css";
import CountryCard from "../components/CountryCard";
import { ReactComponent as SearchSVG } from '../images/search.svg';
const FirstQuestion: React.FC = () => {
  const [country, setCountry] = useState<Country>(new Country());
  const [searchTerm, setSearchTerm] = useState<string>("");
  function loadData(): void {
    CountryService.getOneCountry(searchTerm).then((res) => {
      if (res != null) {
        setCountry(res);
      }
    });
  }

  function ShowCard() {
    if (country.name) {
      return <CountryCard country={country} />;
    } else {
      return <h1>No result.</h1>;
    }
  }
  return (
    <div className="FirstQuestionContainer">
      <InputGroup className="mb-3 ml-auto mr-auto mt-3 w-75">
        <FormControl
          aria-describedby="basic-addon1"
          placeholder="Type a country name"
          onChange={(event) => setSearchTerm(event.target.value)}
        />
        <InputGroup.Append>
          <Button onClick={loadData} variant="outline-secondary">
            <SearchSVG />
          </Button>
        </InputGroup.Append>
      </InputGroup>
      <div className="flexContainer">
        <ShowCard />
      </div>
    </div>
  );
};

export default FirstQuestion;
