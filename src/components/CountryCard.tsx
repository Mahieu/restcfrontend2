import React from "react";
import Card from "react-bootstrap/Card";
import { Country } from "../models/country.model";
import "../styles/CountryCard.css";
type CountryCardProps = {
  country: Country;
};

const CountryCard = ({ country }: CountryCardProps) => {
  return (
    <Card className="countryCard">
      <Card.Img className="countryFlag" variant="top" src={country.flag} />
      <Card.Body>
        <Card.Title>{country.name}</Card.Title>
        <Card.Text>Population : {country.population}</Card.Text>
        <Card.Text>Region : {country.region}</Card.Text>
        <Card.Text>Capital : {country.capital}</Card.Text>
        <Card.Text>Size : {country.area} km²</Card.Text>
      </Card.Body>
    </Card>
  );
};
export default CountryCard;
