import React, { useLayoutEffect } from "react";
import { useDispatch } from "react-redux";
import { checkLogin } from '../slicers/auth.slicer';
import "../styles/App.css";
import Menu from "./Menu";
const App: React.FC = () => {
  const dispatch = useDispatch();
  useLayoutEffect(() => {
    dispatch(checkLogin());
  });
  return (<><Menu /></>);
}
export default App;