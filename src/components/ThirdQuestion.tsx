import React, { useState, useLayoutEffect } from "react";
import "../styles/ThirdQuestion.css";
import { Country } from "../models/country.model";
import FormControl from "react-bootstrap/FormControl";
import CardColumns from "react-bootstrap/CardColumns";
import CountryCard from "./CountryCard";
import { useDispatch, useSelector } from "react-redux";
import { fetchCountries, selectCountries } from '../slicers/countries.slicer';
const ThirdQuestion: React.FC = () => {
  const dispatch = useDispatch();
  const tmpCountries: Country[] = useSelector(selectCountries);
  const [filteredCountries, setFilteredCountries] = useState<Country[]>([]);
  const [searchTerm, setSearchTerm] = useState<string>("");
  let index: number = 1;
  let operation;
  useLayoutEffect(() => {
    if (tmpCountries.length === 0) {
      dispatch(fetchCountries([]));
    }
    if (searchTerm.length > 0 && tmpCountries.length > 0) {
      // eslint-disable-next-line react-hooks/exhaustive-deps
      operation = setTimeout(() => {
        setFilteredCountries(
          tmpCountries.filter((c) => c.name.toLocaleLowerCase().includes(searchTerm))
        );
      }, 750);
    }
  }, [tmpCountries, searchTerm]);
  function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    setSearchTerm(event.target.value.toLocaleLowerCase());
  }
  return (
    <div className="ThirdQuestionContainer">
      <FormControl
        aria-describedby="basic-addon1"
        placeholder="Type to search"
        className="mb-3 ml-auto mr-auto mt-3 w-75"
        onChange={(event) => handleChange(event as any)}
      />
      <CardColumns>
        {filteredCountries.map((c) => (
          <CountryCard key={c.name + index++} country={c} />
        ))}
      </CardColumns>
    </div>
  );
};

export default ThirdQuestion;
