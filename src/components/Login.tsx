import React, { useState } from "react";
import { Form, Button, Container, Col, Row } from 'react-bootstrap';
import Alert from 'react-bootstrap/Alert';
import { LoginDto } from "../dto/login.dto";
import { useDispatch } from "react-redux";
import { login, selectIsLoggedIn } from '../slicers/auth.slicer';
import { useSelector } from 'react-redux';
const Login: React.FC = () => {
  const [email, setEmail] = useState<string>("test@yobetit.mt");
  const [showAlert, setShowAlert] = useState<boolean>(false);
  const [password, setPassword] = useState<string>("YoBetIt007!");
  const isLoggedIn = useSelector(selectIsLoggedIn);
  const dispatch = useDispatch();

  let emailRegex = new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  let passwordRegex = new RegExp(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/);
  function isEmailValid(): boolean {
    return emailRegex.test(email);
  }
  function isPasswordValid(): boolean {
    return passwordRegex.test(password);
  }
  function isFormValid() {
    return isEmailValid() && isPasswordValid();
  }
  function onSubmit(event: React.FormEvent<HTMLFormElement> ): void {
    event.preventDefault();
    let loginDto = new LoginDto();
    loginDto.email = email;
    loginDto.password = password;
    dispatch(login(loginDto));
    setTimeout(() => {
      setShowAlert(true);
    }, 3000);
  }
  return (
    <Container>
      <Row className="mt-5">
        <Col>
          <Form noValidate onSubmit={onSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" placeholder="Enter email" value={email} onChange={(event) => setEmail(event.target.value)} />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" value={password} onChange={(event) => setPassword(event.target.value)} />
            </Form.Group>
            <Button variant="primary" type="submit" disabled={!isFormValid()}>
              Login
    </Button>
          </Form>
        </Col>

      </Row>
      <Row className="mt-5">
        <Col>
          <Alert transition={false} show={!
            isLoggedIn && showAlert} variant='danger'>
            Bad credentials.
          </Alert>
          <Alert transition={false} show={isLoggedIn && showAlert} variant='success'>
            Login succeed.
          </Alert>
        </Col>
      </Row>
    </Container>
  );
}

export default Login;