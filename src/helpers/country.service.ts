import { Country } from "../models/country.model";

export default class CountryService {
    static url: string = "https://restcountriesbackend.herokuapp.com/api/countries";
    public static getCountries(names: string[]) : Promise<Country[]>{
        //If it was a real project, I'll store that in a config file or in an environment variable
        if (names.length > 0 && Array.isArray(names)) {
            let tmpurl = this.url + "?names=" + JSON.stringify(names);
            return fetch(tmpurl).then(res => res.json())
        }
        return fetch(this.url).then(res => res.json() as Promise<any[]>)
    }
    public static getOneCountry(name: string) {
        return fetch(this.url + "/" + name).then(res => res.json() as Promise<any>)
    }
}