import { LoginDto } from "../dto/login.dto";
import { AuthResultDto } from "../dto/auth-result.dto";

export default class AuthService {
  static url: string = "https://restcountriesbackend.herokuapp.com/api/auth";
  public static signIn(infos: LoginDto): Promise<AuthResultDto>{
    let tmpUrl = this.url + "/signin";
    return fetch(tmpUrl, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(infos)
    }).then(res => res.json());
  }
  public static isTokenStillValid(): boolean { 
    console.log(localStorage.getItem('exp'));
    if (localStorage.getItem('exp') === null) {
      return false;
    }
    let expString = (localStorage.getItem('exp'))+"";
    let expDate = new Date(expString);
    return (expDate >= new Date() );
  }
}