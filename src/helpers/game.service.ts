import { Game } from "../models/game.model";

export default class GameService {
    public static getResult() : Promise<Game>{
        let url: string = "https://restcountriesbackend.herokuapp.com/api/games";
        return fetch(url, {
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            })
        }
        ).then(res => res.json());
    }
}