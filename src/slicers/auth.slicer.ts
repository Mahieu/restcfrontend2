import { createSlice, PayloadAction,createAsyncThunk } from '@reduxjs/toolkit';
import { AuthResultDto } from '../dto/auth-result.dto';
import { LoginDto } from '../dto/login.dto';
import AuthService from '../helpers/auth.service';
export type State = {
  status: string;
  isLoggedIn: boolean,
  error: null;
};
export const initialState: State = {
  status: "idle",
  isLoggedIn : false,
  error: null
};
export const login = createAsyncThunk('auth/login', async (creds: LoginDto) => {
  console.log("Login function called in slicer");
  const response = await AuthService.signIn(creds);
  return response as AuthResultDto;
});
export const slice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    checkLogin: (state) => {
      state.status = "succeeded";
      state.isLoggedIn = AuthService.isTokenStillValid();
    },
    setIsLoggedIn: (state, action: PayloadAction<boolean>) => {
      state.isLoggedIn = action.payload;
    }
  },
  extraReducers: builder => {
    builder.addCase(login.pending, (state: State, action: PayloadAction) => {
      state.status = 'loading'
    })
    builder.addCase(login.fulfilled, (state: State, action: PayloadAction<AuthResultDto>) => {
      state.status = 'succeeded';
      state.isLoggedIn = true;
      localStorage.setItem("username", action.payload.username);
      localStorage.setItem("exp", new Date(action.payload.exp).toISOString());
      localStorage.setItem("token", action.payload.token);
      localStorage.setItem("coins", action.payload.coins.toString());
      localStorage.setItem("email", action.payload.email);

    })
  }
});
export const { checkLogin,setIsLoggedIn } = slice.actions;
export const selectIsLoggedIn = (state: any): boolean => {
  console.log(state);
  return state.auth.isLoggedIn ? state.auth.isLoggedIn : false
};
export default slice.reducer;