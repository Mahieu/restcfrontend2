import { createSlice, PayloadAction,createAsyncThunk } from '@reduxjs/toolkit';
import { Country } from '../models/country.model';
import CountryService from '../helpers/country.service';
export type State = {
  status: string;
  countries: Array<Country>;
  error: null;
};
export const initialState: State = {
  status: "idle",
  countries: [],
  error: null
};
export const fetchCountries = createAsyncThunk('countries/fetchCountries', async (names : string[]) => {
  const response = await CountryService.getCountries(names);
  return response as Country[];
});

export const slice = createSlice({
  name: "countries",
  initialState,
  reducers: {
  },
  extraReducers: builder => {
    builder.addCase(fetchCountries.pending, (state: State, action: PayloadAction) => {
      state.status = 'loading'
    })
    builder.addCase(fetchCountries.fulfilled, (state: State, action: PayloadAction<Country[]>) => {
      state.status = 'succeeded';
      state.countries = action.payload.flat();
      console.log("Countries fetched !!!");
    })
  }
});
export const searchCountriesByName = (state : any, name : string) => state.countries.countries.filter((c : Country) => c.name.toLocaleLowerCase().includes(name))
export const selectCountries = (state : any) => state.countries.countries;

export default slice.reducer;